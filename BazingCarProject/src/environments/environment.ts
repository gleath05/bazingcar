// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyBVLuwqjG7NEMV6SO46M8q-BCiIOIYmVAM",
    authDomain: "projecttwo-9709d.firebaseapp.com",
    databaseURL: "https://projecttwo-9709d.firebaseio.com",
    projectId: "projecttwo-9709d",
    storageBucket: "projecttwo-9709d.appspot.com",
    messagingSenderId: "55621094558",
    appId: "1:55621094558:web:e396ebd21786b3b5"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
