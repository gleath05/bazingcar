import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { HomeCarComponent } from './features/product/home-car/home-car.component';
import { ProductComponent } from './features/product/product.component';
import { PromotionComponent } from './features/product/promotion/promotion.component';
import { CarsListComponent } from './features/product/cars-list/cars-list.component';
import { CarsComponent } from './features/product/cars/cars.component';

const appRoutes: Routes = [
  

  { path: '', pathMatch: 'full', redirectTo: 'home' },
  { path: 'home', component: HomeCarComponent },
  { path: 'vehicles', component: ProductComponent },
  { path: 'promotions', component: PromotionComponent },
  { path: 'list-of-cars', component: CarsListComponent },
  { path: 'cars', component: CarsComponent },

];

@NgModule({
  declarations: [],
  imports: [CommonModule, RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
