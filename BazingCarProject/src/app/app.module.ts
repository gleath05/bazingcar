import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MDBBootstrapModule } from 'angular-bootstrap-md';

import { AppComponent } from './app.component';
import { ProductComponent } from './features/product/product.component';

import { CarsListComponent } from './features/product/cars-list/cars-list.component';
import { CarsComponent } from './features/product/cars/cars.component';
import { BenefitsService } from './shared/services/benefits.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { environment } from 'src/environments/environment';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { AppRoutingModule } from './app-routing.module';
import { HomeCarComponent } from './features/product/home-car/home-car.component';
import {AccordionModule} from 'ng-uikit';
import { PromotionComponent } from './features/product/promotion/promotion.component';



@NgModule({
  declarations: [
    AppComponent,
    ProductComponent,
    CarsComponent,
    CarsListComponent,
    HomeCarComponent,
    PromotionComponent,
  ],
  imports: [
    BrowserModule,
    MDBBootstrapModule.forRoot(),
    FormsModule,
    AngularFirestoreModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    BrowserAnimationsModule,
    ToastrModule.forRoot({preventDuplicates: true}),
    AppRoutingModule,
    ReactiveFormsModule,
    AccordionModule
  ],
  providers: [BenefitsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
