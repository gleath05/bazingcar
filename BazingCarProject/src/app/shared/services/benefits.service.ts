import { Injectable } from '@angular/core';
import { Benefits } from '../models/benefits.model';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class BenefitsService {
  formData : Benefits

  constructor(private firestore: AngularFirestore) { }

  getProduct() { 
    return this.firestore.collection('product').snapshotChanges();
  }
  
}
