import { Component, OnInit } from '@angular/core';
import { BenefitsService } from 'src/app/shared/services/benefits.service';
import { Benefits } from 'src/app/shared/models/benefits.model';
import { AngularFirestore } from '@angular/fire/firestore';
import { ToastrService } from 'ngx-toastr';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-cars-list',
  templateUrl: './cars-list.component.html',
  styleUrls: ['./cars-list.component.scss']
})
export class CarsListComponent implements OnInit {

  constructor(private service: BenefitsService, private firestore: AngularFirestore, private toastr: ToastrService) { }
  list: Benefits[];

  ngOnInit() {
    this.service.getProduct().subscribe(actionArray => { 
      this.list = actionArray.map(item => { 
        return {
          id: item.payload.doc.id, 
          ...item.payload.doc.data()
        } as Benefits;
      }) 
    });

    this.resetEditForm();


  }

  resetEditForm(form?: NgForm) {
    if (form != null)
      form.resetForm();
    this.service.formData = {
      id: null,
      vehicle: '',
      price: null,
      color: '',
    }
  }

  onEdit(prod: Benefits) {
    this.service.formData = Object.assign({}, prod);
  }

  onDelete(id: string) {
    if (confirm("Are you sure you want to delete this record? ")) { 
      this.firestore.doc('product/' + id).delete();
      this.toastr.warning('Deleted Successfully','CAR. Register')

    }
  }

  onEditSubmit(form: NgForm) {
    let data = Object.assign({}, form.value);
    delete data.id;
    if (form.value.id == null)
      this.firestore.collection('product').add(data);
    else
      this.firestore.doc('product/' + form.value.id).update(data);
    this.resetEditForm(form);
    this.toastr.success('Edit Successfully', 'CAR. Register');
  }


}
