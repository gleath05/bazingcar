import { Component, OnInit } from '@angular/core';
import { BenefitsService } from 'src/app/shared/services/benefits.service';
import { NgForm } from '@angular/forms';
import { AngularFirestore } from '@angular/fire/firestore';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-cars',
  templateUrl: './cars.component.html',
  styleUrls: ['./cars.component.scss']
})
export class CarsComponent implements OnInit {

  constructor(private service: BenefitsService, private firestore: AngularFirestore, private toastr: ToastrService) { }

  ngOnInit() {
    this.resetForm();
  }
  ngOnDestroy() { 
    this.resetAddForm();
  }
  resetForm(form?: NgForm) {
    if (form != null)
      form.resetForm();
    this.service.formData = {
      id: null,
      vehicle: '',
      price: null,
      color: '',
    }
  }

  resetAddForm() {
    this.service.formData = {
      id: null,
      vehicle: '',
      price: null,
      color: '',
    }
  }


  onSubmit(form: NgForm) {
    let data = Object.assign({}, form.value);
    delete data.id;
    
    if (form.value.id == null)
      this.firestore.collection('product').add(data);
    else
      this.firestore.doc('product/' + form.value.id).update(data);
    this.toastr.success('Submitted Successfully', 'CAR. Register');
    this.resetForm(form);
  }
  
}
